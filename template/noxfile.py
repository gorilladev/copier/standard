"""Nox sessions for the project."""

import nox

nox.options.sessions = ["manifest", "test"]

PYTHON_SOURCES = ["noxfile.py", "src", "tests"]


@nox.session
def manifest(session):
    """Check the MANIFEST.in"""
    session.install("-c", "requirements/dev.txt", "check-manifest")
    session.run("check-manifest", "--quiet", *session.posargs)


@nox.session
def test(session):
    """Run the unit tests"""
    session.install("-c", "requirements/dev.txt", "pytest")
    session.install(".")
    session.run("pytest", *session.posargs)


@nox.session
def requirements(session):
    """Pin requirements"""

    def pipcompile(outputfile, *args):
        session.run(
            "pip-compile",
            "--quiet",
            "--allow-unsafe",
            "--output-file",
            outputfile,
            *session.posargs,
            *args,
            env={"CUSTOM_COMPILE_COMMAND": f"nox -s {session.name}"},
        )

    session.install("pip-tools")
    pipcompile("requirements/dev.txt", "--strip-extras", "requirements/dev.in")
